import 'package:flutter/material.dart';

class MainLayout extends StatelessWidget {
  final PreferredSizeWidget appBar;
  final Widget child;
  final bool Function() willPopScope;
  final Widget bottomNavigationBar;
  final FloatingActionButton floatingActionButton;

  MainLayout({
    this.appBar,
    this.child,
    this.willPopScope,
    this.bottomNavigationBar,
    this.floatingActionButton,
  });

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (willPopScope != null) {
          return willPopScope();
        }
        return false;
      },
      child: Scaffold(
        appBar: appBar,
        body: Padding(
          padding: const EdgeInsets.only(
            top: 20.0,
          ),
          child: child,
        ),
        bottomNavigationBar: bottomNavigationBar,
      ),
    );
  }
}
