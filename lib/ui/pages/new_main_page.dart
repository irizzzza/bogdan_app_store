import 'package:bogdan_app_store/data/models/news_item_model.dart';
import 'package:bogdan_app_store/res/consts.dart';
import 'package:bogdan_app_store/res/data.dart';
import 'package:bogdan_app_store/ui/layouts/main_layout.dart';
import 'package:bogdan_app_store/ui/pages/detail_page.dart';
import 'package:bogdan_app_store/ui/widgets/game_of_the_day.dart';
import 'package:bogdan_app_store/ui/widgets/main_appbar.dart';
import 'package:bogdan_app_store/ui/widgets/nav_bar.dart';
import 'package:bogdan_app_store/ui/widgets/news_list_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NewMainPage extends StatefulWidget {
  @override
  _NewMainPageState createState() => _NewMainPageState();
}

class _NewMainPageState extends State<NewMainPage> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      width: 375,
      height: 903,
      allowFontScaling: true,
    );
    return MainLayout(
      bottomNavigationBar: NavBar(),
      child: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(
          horizontal: 20.0,
        ),
        child: Column(
          children: <Widget>[
            MainAppBar(),
            AppListItem(
              context,
              stabNewsList[0],
            ),
            AppListItem(
              context,
              stabNewsList[1],
            ),
            AppListItem(
              context,
              stabNewsList[2],
            ),
          ],
        ),
      ),
    );
  }
}

Widget AppListItem(context, NewsItemModel stabNewsListItem) {
  return Container(
    child: GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailPage(stabNewsListItem),
          ),
        );
      },
      child: Hero(
        tag: stabNewsListItem.id,
        child: Card(
          elevation: 0,
          color: Colors.transparent,
          child: stabNewsListItem.typeOfPost == GAME_OF_T_D
              ? GameOfTheDayWid(
                  post: stabNewsList[2],
                )
              : NewsListItem(
                  post: stabNewsListItem,
                ),
        ),
      ),
    ),
  );
}
