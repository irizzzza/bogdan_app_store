import 'package:bogdan_app_store/data/models/news_item_model.dart';
import 'package:bogdan_app_store/res/consts.dart';
import 'package:bogdan_app_store/ui/widgets/game_of_the_day.dart';
import 'package:bogdan_app_store/ui/widgets/game_of_the_day_bio.dart';
import 'package:bogdan_app_store/ui/widgets/game_of_the_day_item.dart';
import 'package:bogdan_app_store/ui/widgets/news_list_item.dart';
import 'package:bogdan_app_store/ui/widgets/shapre_story.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DetailPage extends StatelessWidget {
  final NewsItemModel post;

  DetailPage(this.post);

  static const routeName = '/details';

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      width: 375,
      height: 903,
      allowFontScaling: true,
    );

    return Scaffold(
      body: Stack(
        children: <Widget>[
          CustomScrollView(
            physics: const ClampingScrollPhysics(),
            slivers: <Widget>[
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Hero(
                      tag: 'e1',
                      child: Card(
                        elevation: 0,
                        margin: EdgeInsets.zero,
                        color: Colors.transparent,
                        child: post.typeOfPost == GAME_OF_T_D
                            ? GameOfTheDayWid(
                                post: post,
                                decorationNone: true,
                              )
                            : NewsListItem(
                                post: post,
                                decorationNone: true,
                              ),
                      ),
                    ),
                  ],
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  [
                    Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(
                            20.w,
                          ),
                          child: Column(
                            children: <Widget>[
                              Container(
                                child: Text(
                                  post.titleDescription,
                                ),
                              ),
                              Container(
                                child: Text(
                                  post.titleDescription,
                                ),
                              ),
                              Container(
                                child: Text(
                                  post.titleDescription,
                                ),
                              ),
                            ],
                          ),
                        ),
                        post.typeOfPost != GAME_OF_T_D
                            ? Container()
                            : Positioned(
                                bottom: 20,
                                child: Container(
                                  margin: EdgeInsets.symmetric(
                                   // horizontal: 10.w,
                                    vertical: 5.w,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Colors.grey,
                                    borderRadius: BorderRadius.circular(
                                      10.w,
                                    ),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 10.w,
                                      vertical: 5.w,
                                    ),
                                    child: GameOfTheDayItem(),
                                  ),
                                ),
                              ),
                      ],
                    ),
                    post.typeOfPost != GAME_OF_T_D ? Container() : GameOfTheDayBio(),
                    post.typeOfPost != GAME_OF_T_D
                        ? Container()
                        : Center(
                            child: ShapreStory(),
                          ),
                  ],
                ),
              ),
            ],
          ),
          Positioned(
            left: 8.w,
            top: 14.w,
            child: IconButton(
              icon: Icon(
                Icons.cancel,
              ),
              color: Colors.grey,
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
        ],
      ),
    );
  }
}
