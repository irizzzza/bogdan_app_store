import 'package:bogdan_app_store/res/consts.dart';
import 'package:bogdan_app_store/ui/widgets/nav_bar_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      height: 56.0,
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          NavBarItem(
            icon: Icons.search,
            title: NAV_SEARCH,
          ),
          NavBarItem(
            icon: CupertinoIcons.game_controller_solid,
            title: NAV_ARCADE,
          ),
          NavBarItem(
            icon: CupertinoIcons.news,
            title: NAV_APPS,
          ),
          NavBarItem(
            icon: Icons.settings_cell,
            title: NAV_GAME,
          ),
          NavBarItem(
            icon: Icons.format_align_right,
            color: Colors.blue,
            title: NAV_TODAY,
          ),
        ],
      ),
    );
  }
}
