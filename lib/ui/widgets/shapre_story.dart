import 'package:bogdan_app_store/res/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ShapreStory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      width: 375,
      height: 903,
      allowFontScaling: true,
    );
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 50.h,
      ),
      padding: EdgeInsets.symmetric(
        vertical: 10.w,
        horizontal: 20.w,
      ),
      decoration: BoxDecoration(
        color: Colors.grey.withOpacity(0.2),
        borderRadius: BorderRadius.circular(
          10.w,
        ),
      ),
      child: Text(
        'Share Story',
        style: TextStyle(
          fontSize: 16.w,
          color: NAVI_BAR_ITEM_ACTIVE_COLOR,
        ),
      ),
    );
  }
}
