import 'package:bogdan_app_store/data/models/game_of_the_day.dart';
import 'package:bogdan_app_store/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class GameOfTheDayItem extends StatelessWidget {
  GameOfTheDay post = GameOfTheDay(
    title: 'Wilmot\'s Warehouse',
    price: 4.99,
    description: 'Puzzle',
    iconUrl:
        'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/i/a6e74472-2425-4077-817c-55d42cee8697/ddutqjr-f60e03b3-8845-48d4-a325-d8d89a433a73.png/v1/fill/w_350,h_350,strp/wilmot_s_warehouse_icon__by_iconinja__by_iconinja_ddutqjr-350t.png',
  );

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      width: 375,
      height: 903,
      allowFontScaling: true,
    );
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            height: 50.w,
            width: 80.w,
            alignment: Alignment.centerLeft,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                  100.0,
                ),
                color: WHITE_COLOR,
              ),
              height: 32.w,
              width: 72.w,
              child: Center(
                child: Text(
                  '\$ ${post.price}',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: NAVI_BAR_ITEM_ACTIVE_COLOR,
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: 150.w,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  post.title,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: WHITE_COLOR,
                    fontSize: 16.w,
                  ),
                ),
                Text(
                  post.description,
                  maxLines: 1,
                  style: TextStyle(
                    color: WHITE_COLOR,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 50.w,
            width: 50.w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.w),
              image: DecorationImage(
                image: NetworkImage(
                  post.iconUrl,
                ),
                fit: BoxFit.fill,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
