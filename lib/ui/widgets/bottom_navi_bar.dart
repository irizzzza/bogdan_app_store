import 'package:bogdan_app_store/res/colors.dart';
import 'package:bogdan_app_store/res/consts.dart';
import 'package:flutter/material.dart';

class BottomNaviBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      unselectedItemColor: GRAY_COLOR,
      items: [
        BottomNavigationBarItem(
          backgroundColor: Theme.of(context).primaryColor,
          icon: Icon(
            Icons.search,
          ),
          title: Text(
            NAV_SEARCH,
            style: TextStyle(
              fontSize: 10,
            ),
          ),
        ),
        BottomNavigationBarItem(
          backgroundColor: Theme.of(context).primaryColor,
          icon: Icon(
            Icons.games,
          ),
          title: Text(
            NAV_ARCADE,
            style: TextStyle(
              fontSize: 10,
            ),
          ),
        ),
        BottomNavigationBarItem(
          backgroundColor: Theme.of(context).primaryColor,
          icon: Icon(
            Icons.videogame_asset,
          ),
          title: Text(
            NAV_APPS,
            style: TextStyle(
              fontSize: 10,
            ),
          ),
        ),
        BottomNavigationBarItem(
          backgroundColor: Theme.of(context).primaryColor,
          icon: Icon(
            Icons.airplanemode_active,
          ),
          title: Text(
            NAV_GAME,
            style: TextStyle(
              fontSize: 10,
            ),
          ),
        ),
        BottomNavigationBarItem(
          backgroundColor: Theme.of(context).primaryColor,
          icon: Icon(
            Icons.settings,
            color: Colors.blue,
          ),
          title: Text(
            NAV_TODAY,
            style: TextStyle(
              fontSize: 10,
              color: Colors.blue,
            ),
          ),
        ),
      ],
    );
  }
}
