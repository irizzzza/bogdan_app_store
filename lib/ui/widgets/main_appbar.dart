import 'package:bogdan_app_store/res/consts.dart';
import 'package:bogdan_app_store/res/images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';

class MainAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        CircleAvatar(
          radius: 20.w,
          backgroundImage: NetworkImage(AVATAR_IMAGE),
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text(
              DateFormat.MMMMEEEEd().format(DateTime.now()).toUpperCase(),
              style: TextStyle(fontSize: 16.w,),
            ),
            Text(
              TEXT_TODAY,
              style: TextStyle(
                fontSize: 34.w,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
