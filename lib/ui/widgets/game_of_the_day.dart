import 'package:bogdan_app_store/data/models/news_item_model.dart';
import 'package:bogdan_app_store/res/colors.dart';
import 'package:bogdan_app_store/res/consts.dart';
import 'package:bogdan_app_store/ui/widgets/game_of_the_day_item.dart';
import 'package:bogdan_app_store/ui/widgets/n_daily_list_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class GameOfTheDayWid extends StatefulWidget {
  final NewsItemModel post;
  final bool decorationNone;

  GameOfTheDayWid({
    this.post,
    this.decorationNone = false,
  });

  @override
  _GameOfTheDayWidState createState() => _GameOfTheDayWidState();
}

class _GameOfTheDayWidState extends State<GameOfTheDayWid> {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      width: 375,
      height: 903,
      allowFontScaling: true,
    );
    return Padding(
      padding: widget.decorationNone
          ? EdgeInsets.only(
              bottom: 20.h,
            )
          : EdgeInsets.only(bottom: 20.h, top: 10.h),
      child: Container(
        decoration: widget.post.images.first.imageUrl == EMPTY_STRING
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(
                  15.w,
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(
                      0.3,
                    ),
                    spreadRadius: 7,
                    blurRadius: 10,
                    offset: const Offset(
                      0,
                      10,
                    ),
                  ),
                ],
                color: Colors.white,
              )
            : BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(
                      0.3,
                    ),
                    spreadRadius: 7,
                    blurRadius: 10,
                    offset: const Offset(
                      0,
                      10,
                    ),
                  ),
                ],
                //   color: Colors.blueAccent,
                borderRadius: widget.decorationNone ? BorderRadius.circular(0) : BorderRadius.circular(15.w),
                image: DecorationImage(
                  image: NetworkImage(
                    widget.post.images.first.imageUrl,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
        height: 550.h,
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.only(
            top: 20.0,
            right: 20.0,
            bottom: 20.0,
            left: 20.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Spacer(),
              Padding(
                padding: EdgeInsets.only(
                  left: 100.w,
                ),
                child: Text(
                  widget.post.typeOfPost.toUpperCase(),
                  textAlign: TextAlign.end,
                  style: widget.post.color == Colors.white
                      ? TextStyle(
                          fontSize: 60.h,
                          fontWeight: FontWeight.w500,
                          color: GRAY_COLOR,
                          height: 0.85.w,
                        )
                      : TextStyle(
                          fontSize: 60.h,
                          fontWeight: FontWeight.bold,
                          color: WHITE_COLOR,
                          height: 0.85.w,
                        ),
                ),
              ),
              SizedBox(
                height: 30.h,
              ),
              widget.post.dailyListItems != null
                  ? Container(
                      height: 380.h,
                      child: ListView.builder(
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: 4,
                          itemBuilder: (BuildContext ctx, int index) {
                            return Column(
                              children: <Widget>[
                                NDailyListItem(
                                  dayListItem: widget.post.dailyListItems[index],
                                ),
                                Divider(
                                  endIndent: 60.w,
                                ),
                              ],
                            );
                          }),
                    )
                  : Container(),
              widget.post.gameOfTheDay != null
                  ? Container(
                      child: GameOfTheDayItem(),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
