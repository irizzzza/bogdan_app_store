import 'package:bogdan_app_store/data/models/game_of_the_day.dart';
import 'package:bogdan_app_store/res/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class GameOfTheDayBio extends StatelessWidget {
 GameOfTheDay post = GameOfTheDay(
   title: 'Wilmot\'s Warehouse',
   price: 4.99,
   description: 'Puzzle',
   iconUrl:
       'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/i/a6e74472-2425-4077-817c-55d42cee8697/ddutqjr-f60e03b3-8845-48d4-a325-d8d89a433a73.png/v1/fill/w_350,h_350,strp/wilmot_s_warehouse_icon__by_iconinja__by_iconinja_ddutqjr-350t.png',
 );

  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      width: 375,
      height: 903,
      allowFontScaling: true,
    );
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 20.w,
      ),
      width: double.infinity,
      color: Colors.grey.withOpacity(
        0.18,
      ),
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                32.w,
              ),
              image: DecorationImage(
                image: NetworkImage(
                  post.iconUrl,
                ),
                fit: BoxFit.fitWidth,
              ),
            ),
            width: 100.w,
            height: 100.w,
          ),
          Text(
            post.title,
            style: TextStyle(
              fontSize: 24.w,
            ),
          ),
          Text(post.description),
          Chip(
            label: Text(
              '\$${post.price}',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
            backgroundColor: NAVI_BAR_ITEM_ACTIVE_COLOR,
          ),
        ],
      ),
    );
  }
}
