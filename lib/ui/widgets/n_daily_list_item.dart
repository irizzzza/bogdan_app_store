import 'package:bogdan_app_store/data/models/daily_list_item.dart';
import 'package:bogdan_app_store/res/colors.dart';
import 'package:bogdan_app_store/res/consts.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NDailyListItem extends StatefulWidget {
  final DailyListItem dayListItem;
  final String btnText;

  NDailyListItem({
    this.btnText = GET_TEXT,
    @required this.dayListItem,
  });

  @override
  _NDailyListItemState createState() => _NDailyListItemState();
}

class _NDailyListItemState extends State<NDailyListItem> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) {
        ScreenUtil.init(
          context,
          width: 375,
          height: 903,
          allowFontScaling: true,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            height: 50.w,
            width: 70.w,
            alignment: Alignment.center,
            child: widget.dayListItem.isDownloaded
                ? Container(
                    child: Icon(
                    IconData(
                      0xf407,
                      fontFamily: CupertinoIcons.iconFont,
                      fontPackage: CupertinoIcons.iconFontPackage,
                    ),
                    size: 26.w,
                    color: NAVI_BAR_ITEM_ACTIVE_COLOR,
                  ))
                : Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                        100.0,
                      ),
                      color: Colors.grey.withOpacity(
                        0.20,
                      ),
                    ),
                    height: 22.w,
                    width: 52.w,
                    child: Center(
                      child: Text(
                        widget.btnText,
                        style: TextStyle(
                          color: NAVI_BAR_ITEM_ACTIVE_COLOR,
                        ),
                      ),
                    ),
                  ),
          ),
          Container(
            width: 150.w,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  widget.dayListItem.title,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  widget.dayListItem.description,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
              ],
            ),
          ),
          Container(
            height: 50.w,
            width: 50.w,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                10.w,
              ),
              image: DecorationImage(
                image: NetworkImage(
                  widget.dayListItem.logoUrl,
                ),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
