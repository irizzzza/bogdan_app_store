import 'package:bogdan_app_store/data/models/news_item_model.dart';
import 'package:bogdan_app_store/res/colors.dart';
import 'package:bogdan_app_store/res/consts.dart';
import 'package:bogdan_app_store/ui/widgets/n_daily_list_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class NewsListItem extends StatefulWidget {
  final NewsItemModel post;
  final bool decorationNone;

  NewsListItem({
    @required this.post,
    this.decorationNone = false,
  });

  @override
  _NewsListItemState createState() => _NewsListItemState();
}

class _NewsListItemState extends State<NewsListItem> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) {
        ScreenUtil.init(
          context,
          width: 375,
          height: 903,
          allowFontScaling: true,
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: widget.decorationNone ? const EdgeInsets.only(bottom: 1.0) : EdgeInsets.only(bottom: 20.h, top: 10.h),
        child: Container(
          child: Container(
            decoration: widget.post.images.first.imageUrl == EMPTY_STRING
                ? BoxDecoration(
                    borderRadius: BorderRadius.circular(15.w),
                    boxShadow: widget.decorationNone
                        ? [
                            BoxShadow(
                                // color: Colors.grey.withOpacity(0.3),
                                // spreadRadius: 7,
                                // blurRadius: 10,
                                // offset: Offset(0, 10),
                                ),
                          ]
                        : [],
                    color: Colors.white,
                  )
                : BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(
                          0.3,
                        ),
                        spreadRadius: 7,
                        blurRadius: 10,
                        offset: const Offset(
                          0,
                          10,
                        ), // changes position of shadow
                      ),
                    ],
                    //   color: Colors.blueAccent,
                    borderRadius: widget.decorationNone ? BorderRadius.circular(0.0) : BorderRadius.circular(15.w),
                    image: DecorationImage(
                      image: NetworkImage(
                        widget.post.images.first.imageUrl,
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
            height: 550.h,
            width: double.infinity,
            child: Padding(
              padding: const EdgeInsets.all(
                20.0,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Text(
                    widget.post.typeOfPost.toUpperCase(),
                    style: widget.post.color == Colors.white
                        ? TextStyle(
                            fontSize: 20.h,
                            fontWeight: FontWeight.w500,
                            color: GRAY_COLOR,
                          )
                        : TextStyle(
                            fontSize: 20.h,
                            fontWeight: FontWeight.w500,
                            color: Colors.white70,
                          ),
                  ),
                  Text(
                    widget.post.title,
                    textAlign: TextAlign.end,
                    style: widget.post.color == Colors.white
                        ? TextStyle(
                            fontSize: 32.h,
                            fontWeight: FontWeight.bold,
                            color: BLACK_COLOR,
                          )
                        : TextStyle(
                            fontSize: 32.h,
                            fontWeight: FontWeight.w500,
                            color: WHITE_COLOR,
                          ),
                  ),
                  Spacer(),
                  widget.post.dailyListItems != null
                      ? Container(
                          height: 380.h,
                          child: ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 4,
                              itemBuilder: (BuildContext ctx, int index) {
                                return Column(
                                  children: <Widget>[
                                    NDailyListItem(
                                      dayListItem: widget.post.dailyListItems[index],
                                    ),
                                    Divider(
                                      endIndent: 60.w,
                                    ),
                                  ],
                                );
                              }),
                        )
                      : Container(),
                  widget.post.gameOfTheDay != null
                      ? Container(
                          child: Text(
                            widget.post.gameOfTheDay.description,
                          ),
                        )
                      : Container(),
                  widget.post.typeOfPost == MEET_THE_CREATIVE
                      ? Container(
                          child: Text(
                            widget.post.description,
                            style: TextStyle(
                              fontSize: 14.w,
                              color: WHITE_COLOR,
                            ),
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
