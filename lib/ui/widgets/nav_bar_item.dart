import 'package:flutter/material.dart';

class NavBarItem extends StatelessWidget {
  final String title;
  final Color color;
  final IconData icon;

  NavBarItem({
    this.title,
    this.icon,
    this.color,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Icon(
          icon,
          color: color == null ? Colors.grey : color,
        ),
        Text(
          title,
          style: TextStyle(
            fontSize: 10,
            color: color == null ? Colors.grey : color,
          ),
        ),
      ],
    );
  }
}
