// region [Global]
const String EMPTY_STRING = '';
const String APP_TITLE = 'App Store';
const String TEXT_TODAY = 'Today';
// endregion

const String GAME_OF_T_D = 'GAME OF THE DAY';

const String MEET_THE_CREATIVE = 'MEET THE CREATIVE';

const String NAV_SEARCH = 'Search';
const String NAV_ARCADE = 'Arcade';
const String NAV_APPS = 'Apps';
const String NAV_GAME = 'Game';
const String NAV_TODAY = 'Today';

const String GET_TEXT = 'GET';