//todo list cards
import 'package:bogdan_app_store/data/models/daily_list_item.dart';
import 'package:bogdan_app_store/data/models/game_of_the_day.dart';
import 'package:bogdan_app_store/data/models/image_item.dart';
import 'package:bogdan_app_store/data/models/news_item_model.dart';
import 'package:flutter/material.dart';

List<NewsItemModel> stabNewsList = [
  NewsItemModel(
    id: 'e1',
    images: [
      ImageItem(
        imageUrl:
            'https://images.unsplash.com/photo-1578873375841-468b1557216f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80',
      )
    ],
    typeOfPost: 'MEET THE CREATIVE',
    title: 'He\'s With the Band',
    description:
        'Level up with concert photographer Greg Noire\'s Lightroom pressets.',
    titleDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,'
        ' sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
        ' Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris'
        ' nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in '
        'reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        ' Excepteur sint occaecat cupidatat non proident, sunt in culpa qui'
        ' officia deserunt mollit anim id est laborum.',
  ),
  NewsItemModel(
    id: 'e2',
      color: Colors.white,
      images: [
        ImageItem(
          imageUrl:
              ''
             ///todo 'https://www.imgacademy.com/themes/custom/imgacademy/images/helpbox-contact.jpg',
        )
      ],
      typeOfPost: 'THE DAILY LIST',
      title: 'Shop With Curbside Pickup',
      description:
          'Level up with concert photographer Greg Noire\'s Lightroom pressets.',
      titleDescription: '',
      dailyListItems: [
        DailyListItem(
          title: 'Allset: Food Pickup & Takeout',
          description: 'Order ahead at restaurants',
          logoUrl:
              'https://www.gazprom-neft.com/local/templates/mainframe/images/logo-big-ru.png',
        ),
        DailyListItem(
          title: 'Walmart - shopping & grocery',
          description: 'Shop the whole store and market',
          logoUrl:
              'https://www.usaddress.com/blog/wp-content/uploads/2019/06/Walmart-Logo.jpg',
        ),
        DailyListItem(
          isDownloaded: true,
          title: 'DoorDash - Food Delivery',
          description: 'Restaurant Eats & Drinks To Go',
          logoUrl:
              'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQElS1QpnUZkIZpTebLFeZC4txdyw2ocS0NrA&usqp=CAU',
        ),
        DailyListItem(
          title: 'Starbucks',
          description: 'Order, pay, and earn Stars.',
          logoUrl:
              'https://www.gazprom-neft.com/local/templates/mainframe/images/logo-big-ru.png',
        ),
      ]),
  NewsItemModel(
    id: 'e3',
      images: [
        ImageItem(
          imageUrl:
              'https://cdn.pastemagazine.com/www/articles/2019/08/29/wilmot_warehouse_main.jpg',
        )
      ],
      typeOfPost: 'GAME OF THE DAY',
      title: 'Wilmot\'s Warehouse',
      description: 'Puzzle',
      titleDescription: 'The player controls Wilmot, a worker in a two-dimensional warehouse. Each level begins with a delivery of item boxes that the player must store for later retrieval. The organizational strategy is left entirely to the player.[3] (This can be ambiguous, for example, in the tutorial, the player is asked to sort boxes into "winter" and "hats" but is presented with a box that appears to be a woolen hat.[1]) When the delivery timer expires, customers at a service hatch request specific boxes for the player to retrieve and deliver. Proportionate to the delivery\'s speed, the player receives Progress Stars that can be redeemed for upgrades such as a "dash" ability, the ability to carry more boxes at once,[3] expanded warehouse space,[4] and a timelapse view of the player\'s progress.[5] The variety and quantity of boxes increases with the game\'s duration—into the hundreds[4]—constricting the player\'s navigation of the warehouse aisles.[3] After several rounds (months) of deliveries, the player receives an unlimited period to reorganize the warehouse.[4]',

      gameOfTheDay: GameOfTheDay(
          title: 'Wilmot\'s Warehouse',
          description: 'Puzzle',
          price: 14.99,
          iconUrl: 'https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/i/a6e74472-2425-4077-817c-55d42cee8697/ddutqjr-f60e03b3-8845-48d4-a325-d8d89a433a73.png/v1/fill/w_350,h_350,strp/wilmot_s_warehouse_icon__by_iconinja__by_iconinja_ddutqjr-350t.png')),
  NewsItemModel(
    id: 'e4',
    typeOfPost: 'What to watch',
    title: 'Great Disney+ Shows for\'90s Babies',
    titleDescription: 'Stream all your tween favorites-and their new reboots.',
    description:
        'Stream all your tween favorites-and their new reboots. Loren Ipsum dolar...',
  ),
];
