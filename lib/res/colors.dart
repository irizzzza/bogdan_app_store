import 'package:flutter/material.dart';

const Color WHITE_COLOR = const Color(0xFFFFFFFF);
const Color BLACK_COLOR = const Color(0xFF000000);

const Color GRAY_COLOR =  Colors.grey;
const Color NAVI_BAR_ITEM_ACTIVE_COLOR = Colors.blueAccent;
const Color NAVI_BAR_ITEM_COLOR = Colors.white12;