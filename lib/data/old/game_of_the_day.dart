class GameOfTheDay {
  final double price;
  final String title;
  final String bottomTitle;
  final String bottomDescription;
  final String fullDescription;
  final String iconUrl;
  final String backgroundImageUrl;
  final List<String> contentImages;

  GameOfTheDay(
    this.title,
    this.bottomTitle,
    this.bottomDescription,
    this.price,
    this.fullDescription,
    this.iconUrl,
    this.backgroundImageUrl,
    this.contentImages,
  );
}
