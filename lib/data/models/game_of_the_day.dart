import 'abstract/list_item_abst.dart';

class GameOfTheDay with AbstrListItem {
  final String title;
  final String descriptionTitle;
  final String description;
  final double price;
  final String iconUrl;

  GameOfTheDay({
    this.title,
    this.descriptionTitle,
    this.description,
    this.price,
    this.iconUrl,
  });
}
