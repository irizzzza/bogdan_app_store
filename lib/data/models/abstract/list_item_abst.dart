import 'package:flutter/cupertino.dart';

abstract class AbstrListItem {
   bool isDownloaded;
   String title;
   String description;
   String logoUrl;
}
