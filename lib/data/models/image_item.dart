import 'package:flutter/material.dart';

class ImageItem {
  final String imageUrl;
  final String imageDescription;
  final String color;

  ImageItem({
    @required this.imageUrl,
    this.imageDescription,
    this.color,
  });
}
