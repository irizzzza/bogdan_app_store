import 'package:bogdan_app_store/data/models/daily_list_item.dart';
import 'package:bogdan_app_store/data/models/game_of_the_day.dart';
import 'package:bogdan_app_store/data/models/image_item.dart';
import 'package:flutter/cupertino.dart';

class NewsItemModel {
  final double price;
  final String id;
  final String typeOfPost;
  final String title;
  final String titleDescription;
  final String description;
  final Color color;
  final List<ImageItem> images;
  final List<DailyListItem> dailyListItems;
  final GameOfTheDay gameOfTheDay;
  final String iconUrl;

  NewsItemModel({
    this.price,
    this.id,
    @required this.typeOfPost,
    @required this.title,
    @required this.titleDescription,
    @required this.description,
    this.color,
    this.images,
    this.dailyListItems,
    this.gameOfTheDay,
    this.iconUrl,
  });
}
