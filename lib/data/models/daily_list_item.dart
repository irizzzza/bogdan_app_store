import 'package:flutter/cupertino.dart';

import 'abstract/list_item_abst.dart';

class DailyListItem extends AbstrListItem {
  final bool isDownloaded;
  final String title;
  final String description;
  final String logoUrl;

  DailyListItem({
    this.isDownloaded = false,
    @required this.title,
    @required this.description,
    this.logoUrl,
  });
}
