import 'package:bogdan_app_store/res/consts.dart';
import 'package:bogdan_app_store/ui/pages/new_main_page.dart';
import 'package:flutter/material.dart';


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: APP_TITLE,
      home: NewMainPage(),
    );
  }


}